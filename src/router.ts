import { createRouter, createWebHistory } from 'vue-router'
import { RouteRecordRaw } from 'vue-router'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import('./pages/home/Index.vue'),
  },

  {
    path: '/browse',
    component: () => import('./pages/browse/Index.vue'),
    children: [
      {
        path: 'latest',
        component: () => import('./pages/browse/latest/Index.vue'),
      },
    ],
  },

  {
    path: '/search',
    component: () => import('./pages/search/Index.vue'),
    beforeEnter: (to) => {
      if (!to.query.s) {
        return '/'
      }

      return true
    },
  },

  {
    path: '/movie/:imdbID',
    props: true,
    component: () => import('./pages/movie/Movie.vue'),
    // components: {
    //   default: () => import('./pages/movie/Movie.vue'),
    //   Layout: () => import('./layouts/AppLayout.vue'),
    // },
  },

  {
    path: '/name/:imdbID',
    props: true,
    component: () => import('./pages/NotReadyPage.vue'),
  },

  {
    path: '/tops',
    component: () => import('./pages/NotReadyPage.vue'),
  },

  {
    path: '/genres',
    component: () => import('./pages/NotReadyPage.vue'),
  },

  {
    path: '/moods',
    component: () => import('./pages/NotReadyPage.vue'),
  },

  {
    path: '/watchlist',
    component: () => import('./pages/NotReadyPage.vue'),
  },

  {
    path: '/favorities',
    component: () => import('./pages/NotReadyPage.vue'),
  },

  {
    path: '/:catchAll(.*)',
    component: () => import('./pages/404.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})

export default router

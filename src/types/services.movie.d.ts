export declare type ErrorResponse = {
  Response: 'False';
  Error: string;
}

export declare type GetMovieResponse = IMovie | ErrorResponse

export declare type SearchMovieResponse = {
  Response: 'True';
  Search: IMovieMini[];
  totalResults: number;
} | ErrorResponse

export declare type SearchMoviesParams = {
  s: string | LocationQueryValue | LocationQueryValue[];
  page?: number | LocationQueryValue | LocationQueryValue[];
  type?: string | LocationQueryValue | LocationQueryValue[];
  y?: number | LocationQueryValue | LocationQueryValue[];
}

import { MOVIE_TYPES } from '../utils/constants'

export declare type MovieMiniType = typeof MOVIE_TYPES[number]

export declare type MovieRating = {
  Source: string;
  Value:  string;
}

export declare interface IMovieMini {
  Title:  string;
  Year:   number;
  imdbID: string;
  Type:   MovieMiniType;
  Poster: string | 'N/A';
}

export declare interface IMovie {
  Title:      string;
  Year:       string;
  Rated:      string | 'N/A';
  Released:   string;
  Runtime:    string;
  Genre:      string;
  Director:   string;
  Writer:     string;
  Actors:     string;
  Plot:       string;
  Language:   string;
  Country:    string;
  Awards:     string;
  Poster:     string | 'N/A';
  Ratings:    MovieRating[];
  Metascore:  string | 'N/A';
  imdbRating: string;
  imdbVotes:  string;
  imdbID:     string;
  Type:       string;
  DVD:        string | 'N/A';
  BoxOffice:  string | 'N/A';
  Production: string | 'N/A';
  Website:    string;
  Response:   'True';
}

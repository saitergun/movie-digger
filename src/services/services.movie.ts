import { LocationQueryValue } from 'vue-router'

import {
  GetMovieResponse,
  SearchMovieResponse,
  SearchMoviesParams,
} from '../types/services.movie'

import request from '../utils/request'

export const getMovieByImdbId = async (i: string | LocationQueryValue | LocationQueryValue[]) => {
  try {
    // const response = await request.get<GetMovieResponse>('http://localhost:5173/movie.json', { params: { i } })
    const response = await request.get<GetMovieResponse>('', { params: { i } })

    if (response.data.Response === 'True') {
      return response.data
    } else if (response.data.Response === 'False') {
      throw Error(response.data.Error)
    } else {
      throw Error('Unexpected error occured')
    }
  } catch (error) {
    throw Error('Unexpected error occured')
  }
}

export const searchMovie = async (params: SearchMoviesParams) => {
  try {
    const response = await request.get<SearchMovieResponse>('', { params })

    if (response.data.Response === 'True') {
      return response.data
    } else if (response.data.Response === 'False') {
      throw Error(response.data.Error)
    } else {
      throw Error('Unexpected error occured')
    }
  } catch (error) {
    throw Error('Unexpected error occured')
  }
}

export default {
  getMovieByImdbId,
  searchMovie,
}

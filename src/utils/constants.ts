export const MOVIE_TYPES = [
  'movie',
  'series',
  'episode',
  'game',
] as const

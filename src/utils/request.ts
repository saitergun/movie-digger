import axios, { AxiosInstance } from 'axios'

const request: AxiosInstance = axios.create({
  baseURL: import.meta.env.VITE_APP_API_BASE_URL,

  params: {
    apiKey: import.meta.env.VITE_APP_API_KEY,
  },
})

export default request

import { createApp } from 'vue'

import './styles/app.scss'

import router from './router'

import App from './layouts/App.vue'

const app = createApp(App)

app.use(router)

app.mount('#app')
